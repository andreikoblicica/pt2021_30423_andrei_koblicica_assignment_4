Report with products ordered on 2021-05-18

Title: Crispy Curry-Roasted Chickpeas  -  Times ordered: 1
Title: Crisp Spiced Nuts  -  Times ordered: 1
Title: Quick & Spicy Asian Pickles  -  Times ordered: 2
Title: Scallion Cilantro Pancakes  -  Times ordered: 1
Title: Pear-Cranberry Mincemeat Lattice Pie  -  Times ordered: 5
Title: Indonesian Spice Cake  -  Times ordered: 2
Title: Ginger-Lime Squares  -  Times ordered: 3
Title: Ethiopian Spice Tea  -  Times ordered: 4
Title: Prune Tequila Ice Cream  -  Times ordered: 1
Title: Roasted Root Vegetables with Rosemary  -  Times ordered: 1
Title: Holiday Apple-Raisin Challah  -  Times ordered: 1
Title: Roasted Tomato Sauce  -  Times ordered: 1
Title: Barbecued Shrimp  -  Times ordered: 2
Title: Mango Coconut Ice Cream  -  Times ordered: 1
Title: Polenta Pudding with Blackberry Compote and Mascarpone Cream  -  Times ordered: 1
Title: Quatre-Épices  -  Times ordered: 3
Title: Broiled Lamb Chops with Mint Chimichurri  -  Times ordered: 1
Title: Clam and Oyster Chowder  -  Times ordered: 1
Title: Rustic Lemon Tart (Torta Della Nonna al Limone) With Pine Nut Lace Cookies  -  Times ordered: 1
Title: Citrus Sponge Cake with Strawberries  -  Times ordered: 1
Title: Amaranth and Feta Phyllo Triangles  -  Times ordered: 3
