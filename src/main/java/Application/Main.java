package Application;

import Business.DeliveryService;
import Data.Serializer;
import Presentation.LogInController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.application.Application;

/**
 * Main class that launches the JavaFx Application
 */
public class Main extends Application{


    DeliveryService deliveryService=new DeliveryService();

    /**
     * loads the initial scene onto the primary stage
     * @param primaryStage stage that shows up when the application is launched
     * @throws Exception for invalid .fxml file
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/LogIn.fxml"));
        Parent root = loader.load();
        LogInController logInController=loader.getController();
        logInController.initializeScene(deliveryService);
        primaryStage.setOnCloseRequest(e->closeProgram());
        primaryStage.setTitle("Delivery Service System");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.setResizable(false);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * serializes the data in the delivery service class when the program is closed
     */
    private void closeProgram(){
        Serializer serializer=new Serializer();
        serializer.serializeAccounts(deliveryService.getAccounts());
        serializer.serializeProducts(deliveryService.getMenuItems());
        serializer.serializeOrders(deliveryService.getOrders());
    }

}
