package Business;

import Data.Serializer;

import java.io.Serializable;

/**
 * class that models the account of a user of the application
 * contains an id, username, password and account type
 */
public class Account implements Serializable {
    private int id;
    private String username;
    private String password;
    private AccountType accountType;

    /**
     * constructor with all the account fields
     * @param id unique user identifier
     * @param username unique username
     * @param password account password
     * @param accountType type: administrator, employee, client
     */
    public Account(int id,String username, String password, AccountType accountType) {
        this.id=id;
        this.username = username;
        this.password = password;
        this.accountType = accountType;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
    public AccountType getAccountType() {
        return accountType;
    }
    public String toString(){
        return id+" "+username+" "+password+" "+accountType.toString();
    }
}
