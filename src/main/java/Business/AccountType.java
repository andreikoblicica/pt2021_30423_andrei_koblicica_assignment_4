package Business;

/**
 * enum for the type of user account
 */
public enum AccountType {
    ADMINISTRATOR,
    CLIENT,
    EMPLOYEE
}
