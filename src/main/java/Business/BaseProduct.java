package Business;

import java.io.Serializable;

/**
 * class that models a base product with its corresponding attributes
 */
public class BaseProduct extends MenuItem implements Serializable {

     /**
      * empty constructor
      */
     public BaseProduct(){

     }

     /**
      * constructor with all fields of the product
      * @param title product title
      * @param rating product rating, between 0 and 5
      * @param calories product calories
      * @param proteins product proteins
      * @param fats product fats
      * @param sodium product sodium
      * @param price product price
      */
     public BaseProduct(String title, double rating, int calories, int proteins, int fats, int sodium, int price) {
          super(title, rating, calories, proteins, fats, sodium, price);
     }

     /**
      * used for computing the price of a composite product
      * @return price of this product
      */
     public int computePrice(){
          return super.getPrice();
     }
}
