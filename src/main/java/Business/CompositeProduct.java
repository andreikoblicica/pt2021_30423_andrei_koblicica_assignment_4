package Business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * class that models a product that is composed of several menu items
 */
public class CompositeProduct extends MenuItem implements Serializable {
    private ArrayList<MenuItem> items;

    /**
     * constructor, creates a new composite product from a list of menu items
     * @param items
     * @param title
     */
    public CompositeProduct(ArrayList<MenuItem> items, String title){
        this.items=new ArrayList<>(items);
        super.setPrice(computePrice());
        super.setTitle(title);
        computeDetails();
    }

    /**
     * recursively computes the price of the composite product based on its menu item list
     * @return price
     */
    public int computePrice(){
        int price=0;
        for(MenuItem menuItem: items){
            price+=menuItem.computePrice();
        }
        return price;
    }

    /**
     * recursively computes the rating/calories/proteins/fats/sodium of the composite product based on its menu item list
     */
    public void computeDetails(){
        super.setCalories(0);
        super.setProteins(0);
        super.setFats(0);
        super.setSodium(0);
        for(MenuItem menuItem: items){
            super.setCalories(super.getCalories()+ menuItem.getCalories());
            super.setProteins(super.getProteins()+ menuItem.getProteins());
            super.setFats(super.getFats()+ menuItem.getFats());
            super.setSodium(super.getSodium()+ menuItem.getSodium());
        }
    }

    public ArrayList<MenuItem> getItems() {
        return items;
    }

    /**
     * removes from the list a product that was removed from the menu
     * @param menuItem item to be removed
     */
    public void removeItem(MenuItem menuItem){
        items.remove(menuItem);
        for(MenuItem item: items){
            if(item instanceof CompositeProduct){
                ((CompositeProduct) item).removeItem(menuItem);
            }
        }
        computeDetails();
        super.setPrice(computePrice());
    }
}
