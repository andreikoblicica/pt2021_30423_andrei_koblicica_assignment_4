package Business;

import Data.FileWriter;
import Data.Serializer;
import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * class that encapsulates the menu items, orders and accounts and deals with the operations performed by the clients and administrator
 * @invariant isWellFormed()
 */
public class DeliveryService extends Observable implements IDeliveryServiceProcessing,Serializable{

    private ArrayList<Account> accounts;
    private ArrayList<MenuItem> menuItems;
    private Map<Order,ArrayList<MenuItem>> orders;
    private Map<Order,ArrayList<MenuItem>> newOrders;

    /**
     * constructor, initializes the accounts, menu items and orders with data serialized in files
     */
    public DeliveryService(){
        Serializer serializer=new Serializer();
        this.accounts= serializer.deserializeAccounts();
        this.menuItems=serializer.deserializeProducts();
        this.orders=serializer.deserializeOrders();
        newOrders=new HashMap<>();
        assert isWellFormed();
    }



    @Override
    public void importProducts() {
        try{
            File file=new File("products.csv");
            InputStream inputStream=new FileInputStream(file);
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
            ArrayList<MenuItem> items=(ArrayList<MenuItem>) bufferedReader.lines().skip(1).map(mapToItem).collect(Collectors.toList());
            menuItems= (ArrayList<MenuItem>) items.stream().filter(distinctByKey(MenuItem::getTitle)).collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * function that maps a line from the csv file to a base product
     */
    private Function<String, MenuItem> mapToItem = (line) -> {
        String[] p = line.split(",");
        MenuItem menuItem = new BaseProduct(p[0],Double.parseDouble(p[1]),Integer.parseInt(p[2]),Integer.parseInt(p[3]),Integer.parseInt(p[4]),Integer.parseInt(p[5]),Integer.parseInt(p[6]));
        return menuItem;
    };

    /**
     * function that filters duplicates based on product title
     * @param keyExtractor key extractor
     * @param <T> type
     * @return predicate necessary for the stream filter
     */
    public <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    @Override
    public void addNewItem(MenuItem menuItem){
        assert menuItem!=null;
        int preSize=menuItems.size();
        menuItems.add(menuItem);
        assert menuItems.size()==preSize+1;
    }

    @Override
    public void removeMenuItem(MenuItem menuItem){
        assert menuItem!=null;
        int preSize=menuItems.size();
        menuItems.remove(menuItem);
        for(MenuItem item: menuItems){
            if(item instanceof CompositeProduct){
                ((CompositeProduct) item).removeItem(menuItem);
            }
        }
        assert menuItems.size()==preSize-1;
    }

    @Override
    public void generateTimeIntervalReport(int minHour, int maxHour) {
        assert minHour<=23 && minHour>=0 && maxHour<=23 && maxHour>=0 && minHour<=maxHour;
        ArrayList<Order> filteredOrders= (ArrayList<Order>) orders.keySet().stream().filter(o->o.getDate().getHours()>=minHour&&o.getDate().getHours()<=maxHour).collect(Collectors.toList());
        System.out.println(filteredOrders.size());
        FileWriter fileWriter=new FileWriter();
        fileWriter.generateTimeIntervalReportFile(filteredOrders, minHour, maxHour);
    }

    @Override
    public void generateProductsMoreThanValueReport(int value) {
        assert value>=0;
        ArrayList<MenuItem> allItems= (ArrayList<MenuItem>) orders.values().stream().flatMap(e->e.stream()).collect(Collectors.toList());
        Map<MenuItem, Long> allItemsWithAppearances=allItems.stream().collect(Collectors.groupingBy(e->e,Collectors.counting()));
        Map<MenuItem, Long> filteredItemsWithAppearances=allItemsWithAppearances.entrySet().stream().filter(e->e.getValue()>=value).collect(Collectors.toMap(e->e.getKey(),e->e.getValue()));
        FileWriter fileWriter=new FileWriter();
        fileWriter.generateProductsReportFile(filteredItemsWithAppearances,value);
    }

    @Override
    public void generateClientsReport(int minOrders, int minPrice) {
        assert minOrders>=0 && minPrice>=0;
        ArrayList<Order> filteredOrders= (ArrayList<Order>) orders.keySet().stream().filter(e->e.getPrice()>minPrice).collect(Collectors.toList());
        ArrayList<Integer> clientIds= (ArrayList<Integer>) filteredOrders.stream().map(Order::getClientId).collect(Collectors.toList());
        Map<Integer, Long> clientIdsAndNrOfOrders=clientIds.stream().collect(Collectors.groupingBy(e->e,Collectors.counting()));
        FileWriter fileWriter=new FileWriter();
        fileWriter.generateClientsReportFile(clientIdsAndNrOfOrders,accounts, minOrders, minPrice);
    }

    @Override
    public void generateProductsByDayReport(Date date) {
        assert date!=null;
        int preSize= orders.size();
        Map<Order,ArrayList<MenuItem>> filteredOrders= orders.entrySet().stream().filter(e->e.getKey().getDate().getDay()==date.getDay()&&e.getKey().getDate().getMonth()==date.getMonth()&&e.getKey().getDate().getYear()==date.getYear()).collect(Collectors.toMap(e->e.getKey(),e->e.getValue()));
        ArrayList<MenuItem> allItems= (ArrayList<MenuItem>) filteredOrders.values().stream().flatMap(e->e.stream()).collect(Collectors.toList());
        Map<MenuItem, Long> distinctItemsWithAppearances=allItems.stream().collect(Collectors.groupingBy(e->e,Collectors.counting()));
        FileWriter fileWriter=new FileWriter();
        fileWriter.generateProductsByDayReportFile(distinctItemsWithAppearances,date);
        assert orders.size()==preSize+1;
    }


    @Override
    public void createOrder(Order order, ArrayList<MenuItem> cartItems, String username) {
        assert order!=null && cartItems!=null && cartItems.size()>0 && username!=null;
        int preSize= orders.size();
        ArrayList<MenuItem> orderItems=new ArrayList<>(cartItems);
        orders.put(order,orderItems);
        newOrders.put(order,orderItems);
        FileWriter fileWriter=new FileWriter();
        fileWriter.createBill(order, orderItems, username);
        assert orders.size()==preSize+1;
    }

    @Override
    public ArrayList<MenuItem> searchProducts(SearchCriteria searchCriteria) {
        assert  searchCriteria!=null;
        ArrayList<MenuItem> filteredItems= (ArrayList<MenuItem>) menuItems.stream().filter(e->e.getRating()>=searchCriteria.getMinRating() &&e.getRating()<=searchCriteria.getMaxRating()
                &&e.getCalories()>=searchCriteria.getMinCalories()&&e.getCalories()<=searchCriteria.getMaxCalories()
                &&e.getProteins()>=searchCriteria.getMinProteins()&&e.getProteins()<=searchCriteria.getMaxProteins()
                &&e.getFats()>=searchCriteria.getMinFats()&&e.getFats()<=searchCriteria.getMaxFats()
                &&e.getSodium()>=searchCriteria.getMinSodium()&&e.getSodium()<=searchCriteria.getMaxSodium()
                &&e.getPrice()>=searchCriteria.getMinPrice()&&e.getPrice()<=searchCriteria.getMaxPrice()).collect(Collectors.toList());
        if(!searchCriteria.getKeyword().equals("")){
            ArrayList<MenuItem> keywordFilteredItems= (ArrayList<MenuItem>) filteredItems.stream().filter(e->e.getTitle().contains(searchCriteria.getKeyword())).collect(Collectors.toList());
            return keywordFilteredItems;
        }
        return filteredItems;
    }

    /**
     * notifies observers when the employee logs in
     */
    public void checkForUpdates() {
        setChanged();
        notifyObservers(newOrders);
    }


    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

    public Map<Order, ArrayList<MenuItem>> getOrders() {
        return orders;
    }


    public Map<Order, ArrayList<MenuItem>> getNewOrders() {
        return newOrders;
    }

    /**
     * invariant method
     * @return checks whether the class is well formed or not
     */
    public boolean isWellFormed(){
        if(accounts==null) return false;
        if(orders==null) return false;
        if(menuItems==null) return false;
        return true;
    }


}
