package Business;

import java.util.ArrayList;
import java.util.Date;

public interface IDeliveryServiceProcessing {
    /**
     * extracts the base products from the csv file using streams and initializes the list of menu items
     * @pre true
     * @post @nochange
     */
    void importProducts();

    /**
     * adds a new item to the list of menu items
     * @param menuItem item added to menu
     * @pre menuItem!=null
     * @post menuItems.size()==menuItems.size()@pre+1
     */
    void addNewItem(MenuItem menuItem);

    /**
     * removes an item received as parameter from the list of menu items
     * @param menuItem item to be removed
     * @pre menuItem!=null
     * @post menuItems.size()==menuItems,size()@pre+1
     */
    void removeMenuItem(MenuItem menuItem);

    /**
     * generates report with the orders performed between a given start hour and a given end hour regardless the date
     * @param minHour start hour specified
     * @param maxHour end hour specified
     * @pre minHour>=0 && minHour<=23 && maxHour>=0 && maxHour<=23 && minHour<=maxHour
     * @post @nochange
     */
    void generateTimeIntervalReport(int minHour, int maxHour);

    /**
     * generates report with the products ordered more than a specified number of times so far
     * @param value value specified
     * @pre value>=0
     * @post @nochange
     */
    void generateProductsMoreThanValueReport(int value);

    /**
     *  generates report with the clients that have ordered more than a specified number of times and the value of the order was higher than a specified amount
     * @param minOrders minimum number of orders specified
     * @param minPrice minimum price of the orders specified
     * @pre minOrders>=0 && minPrice>=0
     * @post @nochange
     */
    void generateClientsReport(int minOrders, int minPrice);

    /**
     * generates report with the products ordered within a specified day with the number of times they have been ordered
     * @param date date specified
     * @pre date!=null
     * @post @nochange
     */
    void generateProductsByDayReport(Date date);

    /**
     * creates a new order by mapping the order details to the shopping cart items and adds it to the list of orders
     * @param order order details
     * @param cartItems menu items ordered
     * @param username username of the client that ordered
     * @pre order!=null && cartItems!=null && username!=null && cartItems.size()>0
     * @post orders.size()==orders.size()@pre+1
     */
    void createOrder(Order order, ArrayList<MenuItem> cartItems, String username);

    /**
     * filters the menu based on the inserted keyword/rating/price/calories/fats/proteins/sodium
     * @param searchCriteria search criteria inserted by the user
     * @return list of menu items that match the search
     * @pre searchCriteria!=null
     * @post @nochange
     */
    ArrayList<MenuItem> searchProducts(SearchCriteria searchCriteria);
}
