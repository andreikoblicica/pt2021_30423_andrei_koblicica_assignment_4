package Business;

import java.io.Serializable;

/**
 * class that models a product from the restaurant menu with its corresponding fields
 */
public class MenuItem implements Serializable {
    private String title;
    private double rating=0;
    private int calories=0;
    private int proteins=0;
    private int fats=0;
    private int sodium=0;
    private int price=0;

    /**
     * constructor with all product fields
     * @param title product title
     * @param rating product rating, between 0 and 5
     * @param calories product calories
     * @param proteins product proteins
     * @param fats product fats
     * @param sodium product sodium
     * @param price product price
     */
    public MenuItem(String title, double rating, int calories, int proteins, int fats, int sodium, int price) {
        this.title = title;
        this.rating = rating;
        this.calories = calories;
        this.proteins = proteins;
        this.fats = fats;
        this.sodium = sodium;
        this.price = price;
    }

    /**
     * empty constructor
     */
    public MenuItem() {
    }
    public int computePrice(){
        return price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getProteins() {
        return proteins;
    }

    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    public int getFats() {
        return fats;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }

    public int getSodium() {
        return sodium;
    }

    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public String toString(){
        return title+" "+rating+" "+calories+" "+proteins+" "+fats+" "+sodium+" "+price;
    }
}
