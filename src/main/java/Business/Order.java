package Business;

import java.io.Serializable;
import java.util.Date;

/**
 * class that contains the details of a placed order: id of client, id of order, date and total price
 */
public class Order implements Serializable {
    private int orderId;
    private int clientId;
    private Date date;
    private int price;

    /**
     * constructor with all fields
     * @param orderId unique id of order
     * @param clientId unique id of client
     * @param date order date
     * @param price total order price
     */
    public Order(int orderId, int clientId, Date date, int price) {
        this.orderId = orderId;
        this.clientId = clientId;
        this.date = date;
        this.price=price;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getClientId() {
        return clientId;
    }

    public Date getDate() {
        return date;
    }

    public int getPrice() {
        return price;
    }

    /**
     * computes the hash code for mapping the orders to the list of menu items
     * @return
     */
    @Override
    public int hashCode(){
        int hashCode=19;
        hashCode=41*hashCode+orderId;
        hashCode=41*hashCode+clientId;
        hashCode=41*hashCode+price;
        hashCode=41*hashCode+date.hashCode();
        return hashCode;
    }

    /**
     * checks whether an object is equal to this object
     * @param object object checked
     * @return boolean answer
     */
    @Override
    public boolean equals(Object object){
        if (object == this) return true;
        if (!(object instanceof Order)) {
            return false;
        }
        Order order = (Order) object;
        return order.getOrderId()==orderId && order.getClientId()==clientId && order.getDate().equals(date);
    }
}
