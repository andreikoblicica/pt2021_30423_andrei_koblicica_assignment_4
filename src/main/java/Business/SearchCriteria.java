package Business;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * class that encapsulates the criteria used for searching products
 */
public class SearchCriteria {
    private String keyword;
    private double minRating;
    private double maxRating;
    private int minCalories;
    private int maxCalories;
    private int minProteins;
    private int maxProteins;
    private int minFats;
    private int maxFats;
    private int minSodium;
    private int maxSodium;
    private int minPrice;
    private int maxPrice;

    /**
     * constructor, initializes the search criteria with the minimum and maximum values from the list of menu items
     * @param menuItems menu items to be filtered
     */
    public SearchCriteria(ArrayList<MenuItem> menuItems) {
        keyword="";
        minRating=menuItems.stream().min(Comparator.comparingDouble(MenuItem::getRating)).get().getRating();
        maxRating=menuItems.stream().max(Comparator.comparingDouble(MenuItem::getRating)).get().getRating();
        minCalories=menuItems.stream().min(Comparator.comparingInt(MenuItem::getCalories)).get().getCalories();
        maxCalories=menuItems.stream().max(Comparator.comparingInt(MenuItem::getCalories)).get().getCalories();
        minProteins=menuItems.stream().min(Comparator.comparingInt(MenuItem::getProteins)).get().getProteins();
        maxProteins=menuItems.stream().max(Comparator.comparingInt(MenuItem::getProteins)).get().getProteins();
        minFats=menuItems.stream().min(Comparator.comparingInt(MenuItem::getFats)).get().getFats();
        maxFats=menuItems.stream().max(Comparator.comparingInt(MenuItem::getFats)).get().getFats();
        minSodium=menuItems.stream().min(Comparator.comparingInt(MenuItem::getSodium)).get().getSodium();
        maxSodium=menuItems.stream().max(Comparator.comparingInt(MenuItem::getSodium)).get().getSodium();
        minPrice=menuItems.stream().min(Comparator.comparingInt(MenuItem::getPrice)).get().getPrice();
        maxPrice=menuItems.stream().max(Comparator.comparingInt(MenuItem::getPrice)).get().getPrice();
    }

    /**
     * sets the values of all fields
     * @param keyword word to be contained in the filtered items title
     * @param minRating minimum rating of the filtered items
     * @param maxRating maximum rating of the filtered items
     * @param minCalories minimum calories of the filtered items
     * @param maxCalories maximum calories of the filtered items
     * @param minProteins minimum proteins of the filtered items
     * @param maxProteins maximum proteins of the filtered items
     * @param minFats minimum fats of the filtered items
     * @param maxFats maximum fats of the filtered items
     * @param minSodium minimum sodium of the filtered items
     * @param maxSodium maximum sodium of the filtered items
     * @param minPrice minimum price of the filtered items
     * @param maxPrice maximum price of the filtered items
     */
    public void setValues(String keyword, double minRating, double maxRating, int minCalories, int maxCalories, int minProteins, int maxProteins, int minFats, int maxFats, int minSodium, int maxSodium, int minPrice, int maxPrice) {
        this.keyword = keyword;
        this.minRating = minRating;
        this.maxRating = maxRating;
        this.minCalories = minCalories;
        this.maxCalories = maxCalories;
        this.minProteins = minProteins;
        this.maxProteins = maxProteins;
        this.minFats = minFats;
        this.maxFats = maxFats;
        this.minSodium = minSodium;
        this.maxSodium = maxSodium;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    public String getKeyword() {
        return keyword;
    }

    public double getMinRating() {
        return minRating;
    }

    public double getMaxRating() {
        return maxRating;
    }

    public int getMinCalories() {
        return minCalories;
    }

    public int getMaxCalories() {
        return maxCalories;
    }

    public int getMinProteins() {
        return minProteins;
    }

    public int getMaxProteins() {
        return maxProteins;
    }

    public int getMinFats() {
        return minFats;
    }

    public int getMaxFats() {
        return maxFats;
    }

    public int getMinSodium() {
        return minSodium;
    }

    public int getMaxSodium() {
        return maxSodium;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }
}
