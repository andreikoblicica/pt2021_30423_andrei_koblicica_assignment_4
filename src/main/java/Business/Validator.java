package Business;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * class that validates user inputs
 */
public class Validator {
    /**
     * checks whether the inserted username and password exist in the account list
     * @param accounts list of all existing accounts
     * @param username inserted username
     * @param password inserted password
     * @return account that matches the inserted values, or null otherwise
     */
    public Account validateAccount(ArrayList<Account> accounts, String username, String password){
        for(Account account: accounts){
            if (account.getUsername().equals(username)&&account.getPassword().equals(password)){
                return account;
            }
        }
        return null;
    }

    /**
     * checks whether the client can register with the inserted username or it already exists
     * @param accounts list of all existing accounts
     * @param account account the user wants to create
     * @return true if the validation is successful, false otherwise
     */
    public boolean validateRegister(ArrayList<Account> accounts,Account account){
        for(Account a: accounts){
            if(a.getUsername().equals(account.getUsername())){
                return false;
            }
        }
        return true;
    }

    /**
     * checks whether the title of a newly created menu item already exists in the menu
     * @param menuItems list of all existing menu items
     * @param menuItemTitle title of the newly created item
     * @return true if the item can be inserted in the menu, false otherwise
     */
    public boolean validateProductTitle(ArrayList<MenuItem> menuItems,String menuItemTitle){
        int result=menuItems.stream().filter(p->menuItemTitle.equals(p.getTitle())).collect(Collectors.toList()).size();
        if(result!=0){
            return false;
        }
        return true;
    }

    /**
     * checks whether the details inserted by the user are correct
     * @param menuItem item newly created
     * @return true if the details are correct, false otherwise
     */
    public boolean validateProductDetails(MenuItem menuItem){
        if(menuItem.getRating()<0 || menuItem.getRating()>5 || menuItem.getCalories()<=0 || menuItem.getProteins()<0 || menuItem.getFats()<0 || menuItem.getSodium()<0 || menuItem.getPrice()<=0){
            return false;
        }
        return true;
    }
}
