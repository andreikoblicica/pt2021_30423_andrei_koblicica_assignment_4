package Data;

import Business.Account;
import Business.MenuItem;
import Business.Order;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * class used for writing in txt files
 */
public class FileWriter {

    SimpleDateFormat simpleDateTimeFormat=new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");

    /**
     * creates a bill for a newly placed order
     * @param order order details places
     * @param orderItems menu items ordered
     * @param username username of client who placed the order
     */
    public void createBill(Order order,ArrayList<MenuItem> orderItems, String username) {
        File file=new File("D:\\PT2021\\pt2021_30423_andrei_koblicica_assignment_4\\orders\\"+"order"+order.getOrderId()+".txt");
        try {
            java.io.FileWriter fileWriter=new java.io.FileWriter(file);
            fileWriter.write("This is a bill for order #"+order.getOrderId()+"\n\n");
            fileWriter.write("Client id: "+order.getClientId()+"\n");
            fileWriter.write("Client username: "+username+"\n");
            fileWriter.write("Order date and time: "+order.getDate()+"\n");
            fileWriter.write("Products ordered:\n");
            for(MenuItem menuItem: orderItems) {
                fileWriter.write(menuItem.getTitle()+" -- Price: "+menuItem.getPrice()+"\n");
            }
            fileWriter.write("\nTotal price: "+order.getPrice());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * generates txt file with the orders performed between a given start hour and a given end hour regardless the date
     * @param orders
     * @param minHour
     * @param maxHour
     */
    public void generateTimeIntervalReportFile(ArrayList<Order> orders, int minHour, int maxHour){
        File file=new File("D:\\PT2021\\pt2021_30423_andrei_koblicica_assignment_4\\reports\\"+"report1-"+simpleDateTimeFormat.format(new Date())+".txt");
        try {
            java.io.FileWriter fileWriter=new java.io.FileWriter(file);
            fileWriter.write("Report with orders performed between hours "+minHour+" and "+maxHour+":\n\n");
            for(Order order: orders){
                fileWriter.write("ID: "+order.getOrderId()+" -  client ID: "+order.getClientId()+" - Total price: "+order.getPrice()+" - Date and time: "+simpleDateTimeFormat.format(order.getDate())+"\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * generates txt file with the products ordered more than a specified number of times so far
     * @param filteredItemsWithAppearances map containing the necessary items and their number of appearances
     * @param value value used for filtering
     */
    public void generateProductsReportFile(Map<MenuItem, Long> filteredItemsWithAppearances, int value){
        File file=new File("D:\\PT2021\\pt2021_30423_andrei_koblicica_assignment_4\\reports\\"+"report2-"+simpleDateTimeFormat.format(new Date())+".txt");
        try {
            java.io.FileWriter fileWriter=new java.io.FileWriter(file);
            fileWriter.write("Report with products that have been ordered more than "+value+" times:\n\n");
            for(MenuItem menuItem:filteredItemsWithAppearances.keySet()){
                fileWriter.write("Title: "+menuItem.getTitle()+" - Appearances: "+filteredItemsWithAppearances.get(menuItem)+"\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * generates txt file with the clients that have ordered more than a specified number of times and the value of the order was higher than a specified amount
     * @param filteredClientIdsAndNrOfOrders map containing the ids of the filtered clients along with the number of times their orders matched the criteria
     * @param accounts list of all existing accounts
     * @param minOrders min number or orders used for filtering
     * @param minPrice min price of orders used for filtering
     */
    public void generateClientsReportFile(Map<Integer,Long> filteredClientIdsAndNrOfOrders, ArrayList<Account> accounts, int minOrders, int minPrice){
        File file=new File("D:\\PT2021\\pt2021_30423_andrei_koblicica_assignment_4\\reports\\"+"report3-"+simpleDateTimeFormat.format(new Date())+".txt");
        try {
            java.io.FileWriter fileWriter=new java.io.FileWriter(file);
            fileWriter.write("Report with clients that have placed at least "+minOrders+" orders with price more than "+ minPrice+":\n\n");
            for(Integer integer: filteredClientIdsAndNrOfOrders.keySet()){
                fileWriter.write("Client ID: "+integer+" -  Client username: "+accounts.get(integer-1).getUsername()+" -  Number of orders: "+filteredClientIdsAndNrOfOrders.get(integer)+"\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * generates txt file with the products ordered within a specified day with the number of times they have been ordered
     * @param distinctItemsWithAppearances
     * @param date
     */
    public void generateProductsByDayReportFile(Map<MenuItem, Long> distinctItemsWithAppearances,Date date){
        File file=new File("D:\\PT2021\\pt2021_30423_andrei_koblicica_assignment_4\\reports\\"+"report4-"+simpleDateTimeFormat.format(new Date())+".txt");
        try {
            java.io.FileWriter fileWriter=new java.io.FileWriter(file);
            fileWriter.write("Report with products ordered on "+simpleDateFormat.format(date)+"\n\n");
            for(MenuItem menuItem: distinctItemsWithAppearances.keySet()){
                fileWriter.write("Title: "+menuItem.getTitle()+" -  Times ordered: "+distinctItemsWithAppearances.get(menuItem)+"\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
