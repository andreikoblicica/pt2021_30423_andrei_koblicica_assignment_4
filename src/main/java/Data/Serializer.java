package Data;

import Business.Account;
import Business.AccountType;
import Business.MenuItem;
import Business.Order;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * used for serializing and deserializing the application data for preserving the state of the delivery service
 */
public class Serializer {
    private static final String accountsFileName="accounts.ser";
    private static final String productsFileName="products.ser";
    private static final String ordersFileName="orders.ser";


    /**
     * saves all the accounts in a file when the application is closed
     * @param accounts accounts saved
     */
    public void serializeAccounts(ArrayList<Account> accounts){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(accountsFileName);
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(accounts);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * loads the accounts from the file at the start of the application
     * @return list of accounts loaded
     */
    public ArrayList<Account> deserializeAccounts(){
        try {
            FileInputStream fileInputStream = new FileInputStream(accountsFileName);
            ObjectInputStream objectInputStream=new ObjectInputStream(fileInputStream);
            ArrayList<Account> accounts=(ArrayList<Account>)objectInputStream.readObject();
            for(Account account: accounts){
                System.out.println(account.toString());
            }
            objectInputStream.close();
            fileInputStream.close();
            return accounts;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * saves all the products in a file when the application is closed
     * @param menuItems list of menu items saved
     */
    public void serializeProducts(ArrayList<MenuItem> menuItems){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(productsFileName);
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(menuItems);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * loads the menu items from the file at the start of the application
     * @return list of menu items loaded
     */
    public ArrayList<MenuItem> deserializeProducts(){
        try {
            FileInputStream fileInputStream = new FileInputStream(productsFileName);
            ObjectInputStream objectInputStream=new ObjectInputStream(fileInputStream);
            ArrayList<MenuItem> menuItems=(ArrayList<MenuItem>)objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return menuItems;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * saves all the orders in a file when the application is closed
     * @param orders accounts saved
     */
    public void serializeOrders(Map<Order,ArrayList<MenuItem>> orders){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(ordersFileName);
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(orders);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * loads the orders from the file at the start of the application
     * @return list of orders loaded
     */
    public Map<Order,ArrayList<MenuItem>> deserializeOrders(){
        try {
            FileInputStream fileInputStream = new FileInputStream(ordersFileName);
            ObjectInputStream objectInputStream=new ObjectInputStream(fileInputStream);
            Map<Order,ArrayList<MenuItem>> orders=(Map<Order,ArrayList<MenuItem>>)objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return orders;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }




}
