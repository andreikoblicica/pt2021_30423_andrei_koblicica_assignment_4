package Presentation;

import Business.*;
import Business.MenuItem;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

public class AdministratorMenuController {

    @FXML
    private Label actionLabel;
    @FXML
    private Label titleLabel;
    @FXML
    private Label ratingLabel;
    @FXML
    private Label caloriesLabel;
    @FXML
    private Label proteinsLabel;
    @FXML
    private Label fatsLabel;
    @FXML
    private Label sodiumLabel;
    @FXML
    private Label priceLabel;

    @FXML
    private Label addedItemsLabel;
    @FXML
    private TextArea addedItemsArea;
    @FXML
    private Button addItemButton;
    @FXML
    private Label newTitleLabel;
    @FXML
    private TextField newTitleField;

    @FXML
    private TextField titleField;
    @FXML
    private TextField ratingField;
    @FXML
    private TextField caloriesField;
    @FXML
    private TextField proteinsField;
    @FXML
    private TextField fatsField;
    @FXML
    private TextField sodiumField;
    @FXML
    private TextField priceField;

    @FXML
    private Button cancelButton;
    @FXML
    private Button submitButton;
    @FXML
    private Button clearButton;

    @FXML
    private Label reportsLabel1;
    @FXML
    private Label reportsLabel2;
    @FXML
    private Label reportsLabel3;
    @FXML
    private Label reportsLabel4;

    @FXML
    private Button generateReport1;
    @FXML
    private Button generateReport2;
    @FXML
    private Button generateReport3;
    @FXML
    private Button generateReport4;

    @FXML
    private TextField minHourField;
    @FXML
    private TextField maxHourField;
    @FXML
    private TextField minTimesProductField;
    @FXML
    private TextField minOrdersField;
    @FXML
    private TextField minPriceField;

    @FXML
    private DatePicker datePicker;


    @FXML
    private TreeTableView<MenuItem> tableView;
    @FXML
    private TreeTableColumn<MenuItem,String> titleColumn;
    @FXML
    private TreeTableColumn<MenuItem,Double> ratingColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> caloriesColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> proteinsColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> sodiumColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> fatsColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> priceColumn;




    private DeliveryService deliveryService;
    private MenuItem selectedItem;
    private TreeItem<MenuItem> selectedTreeItem;
    private ArrayList<MenuItem> addedMenuItems=new ArrayList<>();

    public void initializeScene(DeliveryService deliveryService){
        this.deliveryService=deliveryService;
        initializeTable();
        setVisibility(false);
    }
    private void initializeTable(){
        titleColumn.setCellValueFactory(menuItemStringCellDataFeatures -> new SimpleStringProperty(menuItemStringCellDataFeatures.getValue().getValue().getTitle()));
        ratingColumn.setCellValueFactory(menuItemDoubleCellDataFeatures -> {
            SimpleDoubleProperty simpleDoubleProperty=new SimpleDoubleProperty(menuItemDoubleCellDataFeatures.getValue().getValue().getRating());
            return simpleDoubleProperty.asObject();
        });
        caloriesColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getCalories());
            return simpleIntegerProperty.asObject();
        });
        proteinsColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getProteins());
            return simpleIntegerProperty.asObject();
        });
        fatsColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getFats());
            return simpleIntegerProperty.asObject();
        });
        sodiumColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getSodium());
            return simpleIntegerProperty.asObject();
        });
        priceColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getPrice());
            return simpleIntegerProperty.asObject();
        });
        TreeItem<MenuItem> root=new TreeItem<>(new MenuItem());
        tableView.setRoot(root);
        tableView.setShowRoot(false);
        populateTable();
    }
    private void populateTable(){
        tableView.getRoot().getChildren().removeAll(tableView.getRoot().getChildren());
        for(MenuItem menuItem: deliveryService.getMenuItems()){
            TreeItem<MenuItem> treeItem=new TreeItem<>(menuItem);
            tableView.getRoot().getChildren().add(treeItem);
            if(menuItem instanceof CompositeProduct){
                for(MenuItem menuItem1: ((CompositeProduct) menuItem).getItems()){
                    TreeItem<MenuItem> treeItem1=new TreeItem<>(menuItem1);
                    treeItem.getChildren().add(treeItem1);
                }
            }
        }
    }

    private void setVisibility(boolean visibility){
        titleLabel.setVisible(visibility);
        ratingLabel.setVisible(visibility);
        caloriesLabel.setVisible(visibility);
        proteinsLabel.setVisible(visibility);
        fatsLabel.setVisible(visibility);
        sodiumLabel.setVisible(visibility);
        priceLabel.setVisible(visibility);
        titleField.setVisible(visibility);
        ratingField.setVisible(visibility);
        caloriesField.setVisible(visibility);
        proteinsField.setVisible(visibility);
        fatsField.setVisible(visibility);
        sodiumField.setVisible(visibility);
        priceField.setVisible(visibility);
        cancelButton.setVisible(visibility);
        submitButton.setVisible(visibility);
    }
    private void setGenerateItemVisibility(boolean visibility){
        addedItemsLabel.setVisible(visibility);
        addedItemsArea.setVisible(visibility);
        addItemButton.setVisible(visibility);
        cancelButton.setVisible(visibility);
        submitButton.setVisible(visibility);
        newTitleLabel.setVisible(visibility);
        newTitleField.setVisible(visibility);
    }
    private void setGenerateReportsVisibility(boolean visibility){
        reportsLabel1.setVisible(visibility);
        reportsLabel2.setVisible(visibility);
        reportsLabel3.setVisible(visibility);
        reportsLabel4.setVisible(visibility);
        generateReport1.setVisible(visibility);
        generateReport2.setVisible(visibility);
        generateReport3.setVisible(visibility);
        generateReport4.setVisible(visibility);
        minHourField.setVisible(visibility);
        maxHourField.setVisible(visibility);
        minTimesProductField.setVisible(visibility);
        minOrdersField.setVisible(visibility);
        minPriceField.setVisible(visibility);
        datePicker.setVisible(visibility);
        clearButton.setVisible(visibility);
        cancelButton.setVisible(visibility);

    }
    private void clearFields(){
        titleField.clear();
        ratingField.clear();
        caloriesField.clear();
        proteinsField.clear();
        fatsField.clear();
        sodiumField.clear();
        priceField.clear();
        addedItemsArea.clear();
        newTitleField.clear();

        minHourField.clear();
        maxHourField.clear();
        minTimesProductField.clear();
        minOrdersField.clear();
        minPriceField.clear();
        datePicker.getEditor().clear();
    }

    @FXML
    private void pressImportItems(ActionEvent actionEvent){
        pressCancel(actionEvent);
        deliveryService.importProducts();
        initializeTable();
    }

    @FXML
    private void pressAddBaseProduct(ActionEvent event){
        pressCancel(event);
        setVisibility(true);
        actionLabel.setText("Add Base Product:");

    }
    @FXML
    private void pressEditProduct(ActionEvent event){
        if(tableView.getSelectionModel().getSelectedItem()!=null) {
            if(tableView.getSelectionModel().getSelectedItem().getParent()==tableView.getRoot()) {
                pressCancel(event);
                actionLabel.setText("Edit Product:");
                selectedTreeItem = tableView.getSelectionModel().getSelectedItem();
                selectedItem = selectedTreeItem.getValue();
                if (selectedItem instanceof BaseProduct) {
                    setVisibility(true);
                } else {
                    titleLabel.setVisible(true);
                    ratingLabel.setVisible(true);
                    priceLabel.setVisible(true);
                    titleField.setVisible(true);
                    ratingField.setVisible(true);
                    priceField.setVisible(true);
                    cancelButton.setVisible(true);
                    submitButton.setVisible(true);
                }
                titleField.setText(selectedItem.getTitle());
                ratingField.setText("" + selectedItem.getRating());
                priceField.setText("" + selectedItem.getPrice());
                caloriesField.setText("" + selectedItem.getCalories());
                proteinsField.setText("" + selectedItem.getProteins());
                fatsField.setText("" + selectedItem.getFats());
                sodiumField.setText("" + selectedItem.getSodium());
            }
        }
    }
    @FXML
    private void pressRemoveProduct(ActionEvent event){
        pressCancel(event);
        if(tableView.getSelectionModel().getSelectedItem()!=null){
            if(tableView.getSelectionModel().getSelectedItem().getParent()==tableView.getRoot()) {
                deliveryService.removeMenuItem(tableView.getSelectionModel().getSelectedItem().getValue());
                populateTable();
            }
        }
    }
    @FXML
    private void pressGenerateReports(ActionEvent event){
        pressCancel(event);
        actionLabel.setText("Generate reports:");
        setGenerateReportsVisibility(true);

    }
    @FXML
    private void pressCreateCompositeProduct(ActionEvent event){
        setVisibility(false);
        clearFields();
        actionLabel.setText("Create Composite Product:");
        setGenerateItemVisibility(true);
        addedItemsArea.setText("");
    }
    @FXML
    private void pressSubmit(ActionEvent event){
        if(actionLabel.getText().equals("Create Composite Product:")){
            if(addedItemsArea.getText().equals("")||newTitleField.getText().equals("")){
                loadAlertBox("Make sure you complete all fields!");
                return;
            }
            Validator validator=new Validator();
            if (!validator.validateProductTitle(deliveryService.getMenuItems(), newTitleField.getText())) {
                loadAlertBox("A product with this name already exists!");
                return;
            }
            if(addedMenuItems.size()<2){
                loadAlertBox("Please select at least 2 products!");
                return;
            }
            MenuItem menuItem=new CompositeProduct(addedMenuItems,newTitleField.getText());
            deliveryService.addNewItem(menuItem);
            TreeItem<MenuItem> treeItem=new TreeItem<>(menuItem);
            tableView.getRoot().getChildren().add(treeItem);
            for(MenuItem menuItem1: ((CompositeProduct) menuItem).getItems()){
                TreeItem<MenuItem> treeItem1=new TreeItem<>(menuItem1);
                treeItem.getChildren().add(treeItem1);
            }
            tableView.refresh();
            setGenerateItemVisibility(false);
            addedMenuItems.removeAll(addedMenuItems);
        }
        else {
            if (titleField.getText().equals("") || ratingField.getText().equals("") || caloriesField.getText().equals("") || proteinsField.getText().equals("") || fatsField.getText().equals("") || sodiumField.getText().equals("") || priceField.getText().equals("")) {
                loadAlertBox("Make sure you complete all fields!");
                return;
            }
            String title = titleField.getText();
            try {
                double rating = Double.parseDouble(ratingField.getText());
                int calories = Integer.parseInt(caloriesField.getText());
                int proteins = Integer.parseInt(proteinsField.getText());
                int fats = Integer.parseInt(fatsField.getText());
                int sodium = Integer.parseInt(sodiumField.getText());
                int price = Integer.parseInt(priceField.getText());
                MenuItem menuItem = new BaseProduct(title, rating, calories, proteins, fats, sodium, price);
                Validator validator = new Validator();
                if (!validator.validateProductDetails(menuItem)) {
                    loadAlertBox("Make sure you insert correct values!");
                    return;
                }
                if (actionLabel.getText().equals("Add Base Product:")) {
                    if (!validator.validateProductTitle(deliveryService.getMenuItems(), menuItem.getTitle())) {
                        loadAlertBox("A product with this name already exists!");
                        return;
                    }
                    deliveryService.addNewItem(menuItem);
                    tableView.getRoot().getChildren().add(new TreeItem<>(menuItem));
                } else if (actionLabel.getText().equals("Edit Product:")) {
                    selectedItem.setTitle(title);
                    selectedItem.setRating(rating);
                    selectedItem.setCalories(calories);
                    selectedItem.setProteins(proteins);
                    selectedItem.setFats(fats);
                    selectedItem.setSodium(sodium);
                    selectedItem.setPrice(price);
                    selectedTreeItem.setValue(selectedItem);
                    for(MenuItem item: deliveryService.getMenuItems()){
                        if(item instanceof CompositeProduct){
                            item.setPrice(item.computePrice());
                            ((CompositeProduct) item).computeDetails();
                        }
                    }


                }
                tableView.refresh();
            } catch (NumberFormatException e) {
                loadAlertBox("Make sure you insert correct values!");
            }
            setVisibility(false);
        }
        actionLabel.setText("");
        clearFields();
    }
    @FXML
    private void pressCancel(ActionEvent event){
        setVisibility(false);
        setGenerateItemVisibility(false);
        setGenerateReportsVisibility(false);
        actionLabel.setText("");
        clearFields();
        addedMenuItems.removeAll(addedMenuItems);
    }
    @FXML
    private void pressLogOut(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/LogIn.fxml"));
        Parent root = loader.load();
        LogInController logInController=loader.getController();
        logInController.initializeScene(deliveryService);
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void pressAddItem(ActionEvent event){
        if(tableView.getSelectionModel().getSelectedItem()!=null){
            MenuItem menuItem=tableView.getSelectionModel().getSelectedItem().getValue();
            addedItemsArea.appendText(menuItem.getTitle()+"\n");
            addedMenuItems.add(menuItem);
        }
    }

    @FXML
    private void pressGenerateReport1(ActionEvent event){
        if(minHourField.getText().equals("")||maxHourField.getText().equals("")){
            loadAlertBox("Make sure you complete all fields!");
            return;
        }
        try{
            int minHour=Integer.parseInt(minHourField.getText());
            int maxHour=Integer.parseInt(maxHourField.getText());
            if(minHour<0 || minHour>23 || maxHour<0 || maxHour>23 || minHour>maxHour){
                loadAlertBox("Make sure you insert correct hours!");
                return;
            }
            deliveryService.generateTimeIntervalReport(minHour,maxHour);
            clearFields();
            loadAlertBox("Report generated successfully!");
        }catch(NumberFormatException e){
            loadAlertBox("Make sure you insert numeric values!");
        }
    }
    @FXML
    private void pressGenerateReport2(ActionEvent event){
        if(minTimesProductField.getText().equals("")){
            loadAlertBox("Make sure you complete all fields!");
            return;
        }
        try{
            int minTimesProduct=Integer.parseInt(minTimesProductField.getText());
            deliveryService.generateProductsMoreThanValueReport(minTimesProduct);
            clearFields();
            loadAlertBox("Report generated successfully!");
        }catch(NumberFormatException e){
            loadAlertBox("Make sure you insert numeric values!");
        }
    }
    @FXML
    private void pressGenerateReport3(ActionEvent event){
        if(minOrdersField.getText().equals("")||minPriceField.getText().equals("")){
            loadAlertBox("Make sure you complete all fields!");
            return;
        }
        try{
            int minOrders=Integer.parseInt(minOrdersField.getText());
            int minPrice=Integer.parseInt(minPriceField.getText());
            deliveryService.generateClientsReport(minOrders,minPrice);
            clearFields();
            loadAlertBox("Report generated successfully!");
        }catch(NumberFormatException e){
            loadAlertBox("Make sure you insert numeric values!");
        }
    }
    @FXML
    private void pressGenerateReport4(ActionEvent event){
        if(datePicker.getValue()==null){
            loadAlertBox("Make sure you complete all fields!");
            return;
        }
        try{
            Date date= Date.valueOf(datePicker.getValue());
            deliveryService.generateProductsByDayReport(date);
            loadAlertBox("Report generated successfully!");
        }catch(NumberFormatException e){
            loadAlertBox("Make sure you insert numeric values!");
        }
    }
    @FXML
    private void pressClear(ActionEvent event){
        clearFields();
    }


    private void loadAlertBox(String message){
        Stage stage=new Stage();
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);

        FXMLLoader loader=new FXMLLoader(getClass().getResource("/AlertBox.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        AlertBoxController alertBoxController=loader.getController();
        alertBoxController.initializeLabel(message);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
