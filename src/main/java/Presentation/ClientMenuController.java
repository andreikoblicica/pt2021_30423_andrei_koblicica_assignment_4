package Presentation;

import Business.*;
import Business.MenuItem;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;


public class ClientMenuController {

    @FXML
    private TreeTableView<MenuItem> tableView;
    @FXML
    private TreeTableColumn<MenuItem,String> titleColumn;
    @FXML
    private TreeTableColumn<MenuItem,Double> ratingColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> caloriesColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> proteinsColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> sodiumColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> fatsColumn;
    @FXML
    private TreeTableColumn<MenuItem,Integer> priceColumn;

    @FXML
    private TextField keywordField;
    @FXML
    private TextField minRatingField;
    @FXML
    private TextField maxRatingField;
    @FXML
    private TextField minCaloriesField;
    @FXML
    private TextField maxCaloriesField;
    @FXML
    private TextField minProteinsField;
    @FXML
    private TextField maxProteinsField;
    @FXML
    private TextField minFatsField;
    @FXML
    private TextField maxFatsField;
    @FXML
    private TextField minSodiumField;
    @FXML
    private TextField maxSodiumField;
    @FXML
    private TextField minPriceField;
    @FXML
    private TextField maxPriceField;

    @FXML
    private TextField quantityField;
    @FXML
    private TextArea cartArea;

    @FXML
    private Label priceLabel;


    DeliveryService deliveryService;
    SearchCriteria searchCriteria;
    ArrayList<MenuItem> cartItems;
    int totalPrice;
    Account account;

    public void initializeScene(DeliveryService deliveryService, Account account){
        this.deliveryService=deliveryService;
        this.account=account;
        searchCriteria=new SearchCriteria(deliveryService.getMenuItems());
        initializeTable();
        initializeFields();
        cartItems=new ArrayList<>();
        totalPrice=0;
        priceLabel.setText(""+totalPrice);
    }
    private void initializeTable(){
        titleColumn.setCellValueFactory(menuItemStringCellDataFeatures -> new SimpleStringProperty(menuItemStringCellDataFeatures.getValue().getValue().getTitle()));
        ratingColumn.setCellValueFactory(menuItemDoubleCellDataFeatures -> {
            SimpleDoubleProperty simpleDoubleProperty=new SimpleDoubleProperty(menuItemDoubleCellDataFeatures.getValue().getValue().getRating());
            return simpleDoubleProperty.asObject();
        });
        caloriesColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getCalories());
            return simpleIntegerProperty.asObject();
        });
        proteinsColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getProteins());
            return simpleIntegerProperty.asObject();
        });
        fatsColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getFats());
            return simpleIntegerProperty.asObject();
        });
        sodiumColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getSodium());
            return simpleIntegerProperty.asObject();
        });
        priceColumn.setCellValueFactory(menuItemIntegerCellDataFeatures -> {
            SimpleIntegerProperty simpleIntegerProperty=new SimpleIntegerProperty(menuItemIntegerCellDataFeatures.getValue().getValue().getPrice());
            return simpleIntegerProperty.asObject();
        });
        TreeItem<MenuItem> root=new TreeItem<>(new MenuItem());
        tableView.setRoot(root);
        tableView.setShowRoot(false);
        populateTable(deliveryService.getMenuItems());
    }
    private void populateTable(ArrayList<MenuItem> menuItems){
        tableView.getRoot().getChildren().removeAll(tableView.getRoot().getChildren());
        for(MenuItem menuItem: menuItems){
            TreeItem<MenuItem> treeItem=new TreeItem<>(menuItem);
            tableView.getRoot().getChildren().add(treeItem);
            if(menuItem instanceof CompositeProduct){
                for(MenuItem menuItem1: ((CompositeProduct) menuItem).getItems()){
                    TreeItem<MenuItem> treeItem1=new TreeItem<>(menuItem1);
                    treeItem.getChildren().add(treeItem1);
                }
            }
        }
    }
    private void initializeFields() {
        SearchCriteria searchCriteria = new SearchCriteria(deliveryService.getMenuItems());
        minRatingField.setText("" + searchCriteria.getMinRating());
        maxRatingField.setText("" + searchCriteria.getMaxRating());
        minCaloriesField.setText("" + searchCriteria.getMinCalories());
        maxCaloriesField.setText("" + searchCriteria.getMaxCalories());
        minProteinsField.setText("" + searchCriteria.getMinProteins());
        maxProteinsField.setText("" + searchCriteria.getMaxProteins());
        minFatsField.setText("" + searchCriteria.getMinFats());
        maxFatsField.setText("" + searchCriteria.getMaxFats());
        minSodiumField.setText("" + searchCriteria.getMinSodium());
        maxSodiumField.setText("" + searchCriteria.getMaxSodium());
        minPriceField.setText("" + searchCriteria.getMinPrice());
        maxPriceField.setText("" + searchCriteria.getMaxPrice());
        quantityField.setText("1");
    }
    private void clear(){
        cartArea.clear();
        quantityField.setText("1");
        totalPrice=0;
        priceLabel.setText(""+totalPrice);
        cartItems.removeAll(cartItems);
    }


    @FXML
    private void pressFilter(ActionEvent event){
        if(minRatingField.getText().equals("")||
                maxRatingField.getText().equals("")||
                minCaloriesField.getText().equals("")||
                maxCaloriesField.getText().equals("")||
                minProteinsField.getText().equals("")||
                maxProteinsField.getText().equals("")||
                minFatsField.getText().equals("")||
                maxFatsField.getText().equals("")||
                minSodiumField.getText().equals("")||
                maxSodiumField.getText().equals("")||
                minPriceField.getText().equals("")||
                maxPriceField.getText().equals("")){
            loadAlertBox("Make sure you complete all numeric fields!");
            return;
        }
        try{
            double minRating=Double.parseDouble(minRatingField.getText());
            double maxRating=Double.parseDouble(maxRatingField.getText());
            int minCalories=Integer.parseInt(minCaloriesField.getText());
            int maxCalories=Integer.parseInt(maxCaloriesField.getText());
            int minProteins=Integer.parseInt(minProteinsField.getText());
            int maxProteins=Integer.parseInt(maxProteinsField.getText());
            int minFats=Integer.parseInt(minFatsField.getText());
            int maxFats=Integer.parseInt(maxFatsField.getText());
            int minSodium=Integer.parseInt(minSodiumField.getText());
            int maxSodium=Integer.parseInt(maxFatsField.getText());
            int minPrice=Integer.parseInt(minPriceField.getText());
            int maxPrice=Integer.parseInt(maxPriceField.getText());
            searchCriteria.setValues(keywordField.getText(),minRating,maxRating,minCalories,maxCalories,minProteins,maxProteins,minFats,maxFats,minSodium,maxSodium,minPrice,maxPrice);
            populateTable(deliveryService.searchProducts(searchCriteria));
            tableView.refresh();
        }catch(NumberFormatException e){
            loadAlertBox("Make sure you insert numeric values!");
        }
    }
    @FXML
    private void pressRestoreFilters(ActionEvent event){
        initializeFields();
    }

    @FXML
    private void pressAddToCart(ActionEvent event){
        if(tableView.getSelectionModel().getSelectedItem()!=null){
            if(quantityField.getText().equals("")){
                loadAlertBox("Please insert quantity!");
                return;
            }
            try{
                int quantity=Integer.parseInt(quantityField.getText());
                if(quantity<1){
                    loadAlertBox("Quantity must be a positive integer!");
                    return;
                }
                MenuItem menuItem=tableView.getSelectionModel().getSelectedItem().getValue();
                cartArea.appendText(quantity+"x "+menuItem.getTitle()+"\n");
                totalPrice+=quantity* menuItem.getPrice();
                for(int i=0;i<quantity;i++){
                    cartItems.add(menuItem);
                }
                priceLabel.setText(""+totalPrice);
                quantityField.setText("1");
            }catch(NumberFormatException e){
                loadAlertBox("Quantity must be a positive integer!");
                return;
            }
        }
    }

    @FXML
    private void pressCancel(ActionEvent event){
        clear();
    }

    @FXML
    private void pressFinalizeOrder(ActionEvent event){
        if(cartItems.size()>0) {
            Order order=new Order(deliveryService.getOrders().size()+1,account.getId(),new Date(),totalPrice);
            deliveryService.createOrder(order,cartItems, account.getUsername());
            loadAlertBox("Order placed successfully!");
            clear();
        }else{
            loadAlertBox("Cart is empty!");
        }

    }

    @FXML
    private void pressLogOut(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/LogIn.fxml"));
        Parent root = loader.load();
        LogInController logInController=loader.getController();
        logInController.initializeScene(deliveryService);
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    private void loadAlertBox(String message){
        Stage stage=new Stage();
        if(!message.equals("Order placed successfully!")) {
            stage.setTitle("Error");
        }
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);

        FXMLLoader loader=new FXMLLoader(getClass().getResource("/AlertBox.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        AlertBoxController alertBoxController=loader.getController();
        alertBoxController.initializeLabel(message);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
