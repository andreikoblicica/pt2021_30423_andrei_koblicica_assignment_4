package Presentation;

import Business.DeliveryService;
import Business.MenuItem;
import Business.Order;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class EmployeeMenuController implements Observer {


    @FXML
    private TextArea ordersArea;

    private Map<Order, ArrayList<MenuItem>> orders;
    SimpleDateFormat simpleDateTimeFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ");
    private DeliveryService deliveryService;

    @Override
    public void update(Observable o, Object arg) {
        this.orders= (Map<Order, ArrayList<MenuItem>>) arg;
        refreshOrderArea();
    }


    private void refreshOrderArea(){
        ordersArea.clear();
        for(Order order: orders.keySet()){
            ordersArea.appendText("Order #"+order.getOrderId()+"\n");
            ordersArea.appendText("Date and time: "+simpleDateTimeFormat.format(order.getDate())+"\n");
            ordersArea.appendText("Products:\n");
            for(MenuItem menuItem: orders.get(order)){
                ordersArea.appendText(menuItem.getTitle()+"\n");
            }
            ordersArea.appendText("\n");
        }
    }

    public void initializeScene(DeliveryService deliveryService){
        this.deliveryService=deliveryService;
        this.orders=deliveryService.getNewOrders();
        refreshOrderArea();
    }

    @FXML
    private void pressLogOut(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/LogIn.fxml"));
        Parent root = loader.load();
        LogInController logInController=loader.getController();
        logInController.initializeScene(deliveryService);
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
