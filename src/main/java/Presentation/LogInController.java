package Presentation;

import Business.Account;
import Business.AccountType;
import Business.DeliveryService;
import Business.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class LogInController {

    DeliveryService deliveryService;

    @FXML
    private TextField usernameField;
    @FXML
    private TextField passwordField;
    @FXML
    private Button logInButton;
    @FXML
    private Button registerButton;

    public void initializeScene(DeliveryService deliveryService){
        this.deliveryService=deliveryService;
    }

    @FXML
    private void pressLogIn(ActionEvent event) throws IOException {
        String username=usernameField.getText();
        String password=passwordField.getText();
        Validator validator=new Validator();
        Account account= validator.validateAccount(deliveryService.getAccounts(),username,password);
        if(account==null){
            loadAlertBox("Invalid account details!");
            return;
        }
        FXMLLoader loader;
        if(account.getAccountType()== AccountType.ADMINISTRATOR){
                loader=new FXMLLoader(getClass().getResource("/AdministratorMenu.fxml"));
        }
        else {
            if (account.getAccountType() == AccountType.CLIENT) {
                loader = new FXMLLoader(getClass().getResource("/ClientMenu.fxml"));

            } else {
                loader = new FXMLLoader(getClass().getResource("/EmployeeMenu.fxml"));
            }
        }
        Parent root = loader.load();

        if(account.getAccountType()== AccountType.ADMINISTRATOR){
            AdministratorMenuController administratorMenuController=loader.getController();
            administratorMenuController.initializeScene(deliveryService);
        }
        else {
            if (account.getAccountType() == AccountType.CLIENT) {
                ClientMenuController clientMenuController=loader.getController();
                clientMenuController.initializeScene(deliveryService, account);

            } else {
                EmployeeMenuController employeeMenuController=loader.getController();
                employeeMenuController.initializeScene(deliveryService);
                deliveryService.checkForUpdates();
            }
        }
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

    }
    @FXML
    private void pressRegister(ActionEvent event) throws IOException {
        Validator validator=new Validator();
        String username=usernameField.getText();
        String password=passwordField.getText();
        int id=deliveryService.getAccounts().size()+1;
        Account account=new Account(id,username,password,AccountType.CLIENT);
        if(validator.validateRegister(deliveryService.getAccounts(), account)){
            deliveryService.getAccounts().add(account);
            FXMLLoader loader=new FXMLLoader(getClass().getResource("/ClientMenu.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }
        else{
            loadAlertBox("Username already exists!");
        }
    }

    private void loadAlertBox(String message) throws IOException {
        Stage stage=new Stage();
        stage.setTitle("Error");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader loader=new FXMLLoader(getClass().getResource("/AlertBox.fxml"));
        Parent root = loader.load();
        AlertBoxController alertBoxController=loader.getController();
        alertBoxController.initializeLabel(message);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
    }

}
